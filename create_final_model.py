### Create the final model.
### Traverse the space of models. Combine predictions on the given test set into the final set of predictions
from cluster_utils import output_classification_results, pd


def collect_predictions(results_folder):

    ## Traverse individual results, collect index-prediction maps

    pass


def generate_ensemble_prediction(joint_prediction_space, ensemble_type="mod"):

    ## Mod row-wise to obtain the final predictions + uncertainty?

    pass


if __name__ == "__main__":

    results_folder = "results/*"
    out_file = "./data/test.tsv"

    dfx = pd.read_csv(out_file, sep="\t")
    test_sequences = dfx.text_a.values.tolist()
    test_classes = dfx.label.values

    prediction_structure = collect_predictions(results_folder)
    predictions = generate_ensemble_prediction(prediction_structure)

    ## Output the results + predictions for further use
    output_classification_results(predictions, test_classes,
                                  f"./predictions/ensemble_report.json")
