import secrets
import json

## Jobs specification
GLOBAL_CONFIG_NUM = 10  ## how many different autobot runs do we need?
GLOBAL_CONFIG_BSIZE = 1  ## how many sequential runs are expected?
GLOBAL_CONFIG_FILES = "arccp cluster_pack.zip gsiftp://dcache.arnes.si/data/arnes.si/gen.vo.sling.si/skrlj_virtual_envs/"
GLOBAL_CONFIG_IMAGE = "arccp containers/autobot.sif gsiftp://dcache.arnes.si/data/arnes.si/gen.vo.sling.si/skrlj_virtual_envs/"

## Model specification (learning)
GLOBAL_CONFIG_TRAIN_PATH = "./data/train.tsv"
GLOBAL_CONFIG_TEST_PATH = None #"./data/test.tsv"
GLOBAL_CONFIG_REPTYPE = "neurosymbolic"
GLOBAL_CONFIG_NCPU = 16
GLOBAL_CONFIG_TIME = 8  ## evolution time
GLOBAL_CONFIG_SPARSITY = 0.05
GLOBAL_CONFIG_CV = 8
GLOBAL_CONFIG_NIND = 10
GLOBAL_CONFIG_CROSSOVER_PROB = 0.5
GLOBAL_CONFIG_MUTATION_PROB = 0.5
GLOBAL_LEARNER_PRESET = "default"
GLOBAL_OUTPUT_REPORT = False

## Cluster specs
GLOBAL_CONFIG_CLUSTER_WALLTIME = "1 days"
GLOBAL_CONFIG_CLUSTER_COUNT = "16"
GLOBAL_CONFIG_CLUSTER_COUNTPERNODE = "16"
GLOBAL_CONFIG_CLUSTER_MEMORY = "2000"  ## per cpu!
GLOBAL_CONFIG_CLUSTER_JOB_NAME = "autobot_job_" + str(
    secrets.token_hex(nbytes=9))

## xrsl run file
data = f"""&
(executable = "runfile.sh")
(inputFiles =
  ("runfile.sh" "PLACEHOLDER")
  (*"cluster_pack.zip" "gsiftp://dcache.arnes.si/data/arnes.si/gen.vo.sling.si/skrlj_virtual_envs/cluster_pack.zip"*)
  ("cluster_pack.zip" "../cluster_pack.zip")
  ("autobot.sif" "gsiftp://dcache.arnes.si/data/arnes.si/gen.vo.sling.si/skrlj_virtual_envs/autobot.sif")
)
(outputfiles = 
  ("models.tar.gz" "models.tar.gz")
  ("predictions.tar.gz" "predictions.tar.gz")
  (*"report.tar.gz" "report.tar.gz"*)
)
(walltime = "{GLOBAL_CONFIG_CLUSTER_WALLTIME}")
(memory={GLOBAL_CONFIG_CLUSTER_MEMORY})
(join="yes")
(stdout="job.log")
(count={GLOBAL_CONFIG_CLUSTER_COUNT})
(countpernode={GLOBAL_CONFIG_CLUSTER_COUNTPERNODE})
(*queue = "grid"*)
(cache=no)
(jobname="JID: PLACEHOLDER {GLOBAL_CONFIG_CLUSTER_JOB_NAME}")
"""

## Generate a json-like struct for logging purposes
GLOBAL_JSON_CONFIG = {k: v for k, v in globals().items() if "GLOBAL_" in k}
GLOBAL_JSON_CONFIG['GLOBAL_MODEL_CONFIG'] = json.dumps(GLOBAL_JSON_CONFIG)
