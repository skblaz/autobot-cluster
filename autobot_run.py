"""A simple runfile for multiclass classification - skrlj 2021
"""

from global_config import GLOBAL_JSON_CONFIG
import numpy as np
import autoBOTLib
import pandas as pd
import secrets
from cluster_utils import output_classification_results
import json
import datetime


def run():
    jid = secrets.token_hex(nbytes=16)
    df_path = GLOBAL_JSON_CONFIG['GLOBAL_CONFIG_TRAIN_PATH']

    # Load example data frame
    dataframe = pd.read_csv(df_path, sep="\t")
    dataframe.text_a.values.tolist()

    # Each run must have a different seed
    txt = str(datetime.datetime.now())
    [int(s) for s in txt.split() if s.isdigit()]
    seed = np.random.randint(99992)
    global_model_config = json.loads(GLOBAL_JSON_CONFIG['GLOBAL_MODEL_CONFIG'])
    global_model_config['GLOBAL_RANDOM_SEED'] = seed
    global_model_config['LOCAL_JOB_ID'] = jid
    GLOBAL_JSON_CONFIG['GLOBAL_MODEL_CONFIG'] = global_model_config

    n_cpu = GLOBAL_JSON_CONFIG['GLOBAL_CONFIG_NCPU']
    subframe = dataframe[['text_a', 'label']]
    subframe = subframe.replace('nan', np.NaN)
    subframe = subframe.dropna()
    train_sequences = subframe.text_a.values.tolist()
    train_targets = subframe.label.values.tolist()
    reptype = GLOBAL_JSON_CONFIG['GLOBAL_CONFIG_REPTYPE']
    
    autoBOTObj = autoBOTLib.GAlearner(
        train_sequences,  # input sequences
        train_targets,  # target space
        time_constraint=GLOBAL_JSON_CONFIG[
            'GLOBAL_CONFIG_TIME'],  # time in hours
        sparsity=GLOBAL_JSON_CONFIG['GLOBAL_CONFIG_SPARSITY'],
        n_fold_cv=GLOBAL_JSON_CONFIG['GLOBAL_CONFIG_CV'],
        random_seed=GLOBAL_JSON_CONFIG['GLOBAL_MODEL_CONFIG']
        ['GLOBAL_RANDOM_SEED'],
        representation_type=reptype,
        learner_preset=GLOBAL_JSON_CONFIG['GLOBAL_LEARNER_PRESET'],
        num_cpu=n_cpu)

    autoBOTObj.evolve(
        nind=GLOBAL_JSON_CONFIG['GLOBAL_CONFIG_NIND'],  # population size
        strategy="evolution",  # optimization strategy
        crossover_proba=GLOBAL_JSON_CONFIG[
            'GLOBAL_CONFIG_CROSSOVER_PROB'],  # crossover rate
        mutpb=GLOBAL_JSON_CONFIG['GLOBAL_CONFIG_MUTATION_PROB']
    )  # mutation rate

    autoBOTLib.store_autobot_model(autoBOTObj, f"./models/{jid}__model.pickle")

    # Load the stored model    
    autoBOTObj = autoBOTLib.load_autobot_model(f"./models/{jid}__model.pickle")

    out_file = GLOBAL_JSON_CONFIG['GLOBAL_CONFIG_TEST_PATH']
        
    if not out_file is None:
        
        dfx = pd.read_csv(out_file, sep="\t")
        test_sequences = dfx.text_a.values.tolist()
        test_classes = dfx.label.values

    else:

        # Todo, replace this with a better surrogate if no split is provided
        test_sequences = train_sequences
        test_classes = train_targets

    # Predict
    predictions = autoBOTObj.predict(test_sequences)

    if GLOBAL_JSON_CONFIG['GLOBAL_OUTPUT_REPORT']:
        autoBOTObj.generate_report(output_folder="./report/", job_id=jid)

    # Output the results + predictions for further use
    output_classification_results(
        predictions,
        test_classes,
        f"./predictions/test_{jid}_autobot{reptype}v2_report.json",
        model_spec=GLOBAL_JSON_CONFIG['GLOBAL_MODEL_CONFIG'])

if __name__ == "__main__":

    run()
