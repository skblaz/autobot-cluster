import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

if __name__ == "__main__":
    
    dfx = pd.read_csv("report/overall_report_classification.tsv", sep = "\t")
    sns.scatterplot(dfx['Macro Prec'], dfx['Macro Rec'], hue = dfx['Macro F1'], palette="coolwarm")
    sns.kdeplot(dfx['Macro Prec'], dfx['Macro Rec'], color="black", alpha=0.1)
    plt.savefig("./images/pareto.pdf", dpi = 300)
    plt.clf()
    plt.cla()

    metrics = ['Accuracy','Macro Prec','Macro Rec','Macro F1']
    for metric in metrics:    
        sns.distplot(dfx[[metric]], color="black")
        plt.xlabel(metric)
        plt.ylabel("Density")
        plt.savefig(f"./images/{metric}.pdf", dpi = 300)
        plt.clf()
        plt.cla()
