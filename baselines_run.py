## a script which computes baselines' performance
from baselines import *
from cluster_utils import *
import secrets
import sys
from pathlib import Path
import pandas as pd

Path("./predictions").mkdir(parents=True, exist_ok=True)
Path("./models").mkdir(parents=True, exist_ok=True)
Path("./report").mkdir(parents=True, exist_ok=True)

if __name__ == "__main__":

    all_baselines = dir(sys.modules[__name__])
    jid = secrets.token_hex(nbytes=16)
    df_path = "./data/train.tsv"

    ## Load example data frame
    dataframe = pd.read_csv(df_path, sep="\t")
    final_sequences = dataframe.text_a.values.tolist()

    subframe = dataframe[['text_a', 'label']]
    subframe = subframe.replace('nan', np.NaN)
    subframe = subframe.dropna()
    train_sequences = subframe.text_a.values.tolist()
    train_targets = subframe.label.values.tolist()

    for el in all_baselines:
        if "BASELINE" in el:
            bstring = el + "(train_sequences,train_targets)"
            bname = "-".join(el.split("_")[2:4])
            try:

                print(f"evaluation of {bname}")
                out_file = "./data/test.tsv"
                dfx = pd.read_csv(out_file, sep="\t")
                test_sequences = dfx.text_a.values.tolist()

                baseline_model = eval(bstring)
                if type(baseline_model) == tuple:
                    if "doc2vec" in bname:
                        (clf, model) = baseline_model
                        vecs = []
                        for doc in test_sequences:
                            vector = model.infer_vector(simple_preprocess(doc))
                            vecs.append(vector)
                        test_sequences = vecs
                        baseline_model = clf

                    elif "smbert" in bname:
                        (clf, model) = baseline_model
                        test_sequences = model.encode(test_sequences)
                        baseline_model = clf

                predictions = baseline_model.predict(test_sequences)
                # if "bert" in bname:
                #     predictions, raw_outputs = baseline_model.predict(test_sequences)

                test_classes = dfx.label.values.tolist()
                try:
                    predictions = predictions.tolist()
                except:
                    pass

                ## Output the results + predictions for further use
                output_classification_results(
                    predictions,
                    test_classes,
                    f"./predictions/test_{jid}_{bname}_report.json",
                    print_out=False)
                print(f"written: {bname}")
            except Exception as es:
                print(f"Could not evaluate baseline: {bname}, {es}")
                continue
